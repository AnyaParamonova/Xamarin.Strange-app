﻿using Rg.Plugins.Popup.Services;
using Strange_app.dto;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Strange_app
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PositionsList : ContentPage
    {
        public ObservableCollection<WorkPosition> Positions { get; set; }

        public Command AddPositionCommand { get; set; }

        public PositionsList()
        {
            InitializeComponent();

            Positions = App.Positions;
            AddPositionCommand = new Command<string>(AddPosition);

            this.BindingContext = this;
        }

        public async void OnPositionTapped(object sender, ItemTappedEventArgs e)
        {
            var selectedPosition = e.Item as WorkPosition;
            if (selectedPosition != null)
            {
                await PopupNavigation.PushAsync(new EditPopup(selectedPosition));
            }
        }

        public async void AddPosition(string position)
        {
            await PopupNavigation.PushAsync(new EditPopup());
        }

    }
}