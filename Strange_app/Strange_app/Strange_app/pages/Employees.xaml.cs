﻿using Strange_app.dto;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using Xamarin.Forms;

namespace Strange_app
{
    public partial class Employees : ContentPage
    {
        public ObservableCollection<Employee> EmployeesList { get; set; }

        public Employees()
        {
            InitializeComponent();


            EmployeesList = new ObservableCollection<Employee>()
            {
                new Employee{Name = "Employee 1", Position = App.Positions[0], Description = "Some long text", Avatar = App.Pictures[0] },
                new Employee{Name = "Employee 2", Position = App.Positions[1], Description = "Some long text", Avatar = App.Pictures[1] },
                new Employee{Name = "Employee 3", Position = App.Positions[2], Description = "Some long text", Avatar = App.Pictures[2] },
                new Employee{Name = "Employee 4", Position = App.Positions[3], Description = "Some long text", Avatar = App.Pictures[3] },
                new Employee{Name = "Employee 5", Position = App.Positions[4], Description = "Some long text", Avatar = App.Pictures[4] },
                new Employee{Name = "Employee 6", Position = App.Positions[5], Description = "Some long text", Avatar = App.Pictures[5] },
                new Employee{Name = "Employee 7", Position = App.Positions[6], Description = "Some long text", Avatar = App.Pictures[6] },
                new Employee{Name = "Employee 8", Position = App.Positions[7], Description = "Some long text", Avatar = App.Pictures[7] }
            };
            this.BindingContext = this;
        }

        public async void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            var selectedEmployee = e.Item as Employee;
            if (selectedEmployee != null)
            {
                await Navigation.PushAsync(new EmployeeDetailed(selectedEmployee));
            }
                
        }

    }


}
