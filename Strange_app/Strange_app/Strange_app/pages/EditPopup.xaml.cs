﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Strange_app.dto;
using System;
using Xamarin.Forms.Xaml;

namespace Strange_app
{
    [XamlCompilation(XamlCompilationOptions.Compile)]   
    public partial class EditPopup : PopupPage
    {
        public WorkPosition Position { get; set; }
        private WorkPosition originalPosition;

        public EditPopup()
        {
            InitializeComponent();
            Position = new WorkPosition { Name = "" };
            this.BindingContext = this;
        }

        public EditPopup(WorkPosition position)
        {
            InitializeComponent();
            originalPosition = position;
            Position = new WorkPosition { Name = position.Name };
            this.BindingContext = this;
        }


        public async void Save(object sender, EventArgs args)
        {
            if(originalPosition == null)
            {
                App.Positions.Add(Position);
            }
            else
            {
                originalPosition.Name = Position.Name;
            }
            
            await PopupNavigation.PopAsync();
        }

        public async void Cancel(object sender, EventArgs args)
        {
            await PopupNavigation.PopAsync();
        }
    }
}