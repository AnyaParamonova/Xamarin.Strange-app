﻿using Strange_app.dto;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Strange_app
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EmployeeDetailedEdit : ContentPage
    {
        public Employee Employee { get; set; }

        public Command SaveEmployeeCommand { get; set; }

        public Command ImageTappedCommand { get; set; }

        private Employee originalEmployee;

        public EmployeeDetailedEdit()
        {
            InitializeComponent();
            SaveEmployeeCommand = new Command<Employee>(SaveEmployee);

        }

        public EmployeeDetailedEdit(Employee employee) : this()
        {
            originalEmployee  = employee;
            Employee = new Employee { Name = employee.Name, Position = employee.Position, Description = employee.Description, Avatar = employee.Avatar };
            this.BindingContext = this;
        }

        public async void SaveEmployee(Employee employee)
        {
            await Navigation.PopAsync();

            originalEmployee.Name = Employee.Name;
            originalEmployee.Position = Employee.Position;
            originalEmployee.Avatar = Employee.Avatar;
            originalEmployee.Description = Employee.Description;
        }

        public async void ImageTapped(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new PictureList());
        }

        public void SetPicture(Picture picture)
        {
            Employee.Avatar = picture;
        }
    }
}